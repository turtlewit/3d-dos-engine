#ifndef GAMEPORT_H
#define GAMEPORT_H

#include <dos.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct JoystickValues {
	union {
		uint8_t raw;
		struct {
			uint8_t : 4;
			uint8_t buttons : 4;
		};
		struct {
			uint8_t : 4;
			bool b0 : 1;
			bool b1 : 1;
			bool b2 : 1;
			bool b3 : 1;
		};
	};
	uint16_t axis[4];
} JoystickValues;

inline JoystickValues read_joystick(void)
{
	JoystickValues values = {};

	union REGS regs = {};
	regs.h.ah = 0x84;
	regs.w.dx = 0x00;

	int86(0x15, &regs, &regs);

	values.raw = regs.h.al;

	regs = (union REGS){};
	regs.h.ah = 0x84;
	regs.w.dx = 0x01;
	int86(0x15, &regs, &regs);

	values.axis[0] = regs.w.ax;
	values.axis[1] = regs.w.bx;
	values.axis[2] = regs.w.cx;
	values.axis[3] = regs.w.dx;

	return values;
}

#endif
