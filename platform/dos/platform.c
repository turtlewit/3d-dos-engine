#include <dos.h>
#include <conio.h>
#include <sys/nearptr.h>
#include <stdio.h>

#include "platform/platform.h"

#define INT_VEC_BIOS_VIDEO_SERVICES 0x10
#define BIOS_INT_SET_VIDEO_MODE 0x00

typedef struct Platform {
	uint8_t* pixels;
} Platform;

static Platform platform;


static void video_mode_set_13h(void)
{
	union REGS registers;
	registers.h.ah = BIOS_INT_SET_VIDEO_MODE;
	registers.h.al = 0x13; // mode 13h

	int86(INT_VEC_BIOS_VIDEO_SERVICES, &registers, &registers);

	__djgpp_nearptr_enable();
	platform.pixels = (unsigned char*) 0xA0000 + __djgpp_conventional_base;
}

static void video_mode_set_text(void)
{
	__djgpp_nearptr_disable();

	union REGS registers;
	registers.h.ah = BIOS_INT_SET_VIDEO_MODE;
	registers.h.al = 0x03; // mode 03h, text mode

	int86(INT_VEC_BIOS_VIDEO_SERVICES, &registers, &registers);
}

void platform_init(void)
{
	video_mode_set_13h();
}

void platform_set_palette(uint8_t* palette)
{
	for (int i = 0; i < 256; ++i) {
		const uint32_t uint8_to_32 = 16843009;
		const uint32_t uint32_to_6 = 68174084;
		const int idx = i * 3;
		const uint8_t r = (uint8_t) ((((uint32_t) palette[idx]) * uint8_to_32) / uint32_to_6);
		const uint8_t g = (uint8_t) ((((uint32_t) palette[idx+1]) * uint8_to_32) / uint32_to_6);
		const uint8_t b = (uint8_t) ((((uint32_t) palette[idx+2]) * uint8_to_32) / uint32_to_6);
		outp(0x03c8, i);
		outp(0x03c9, r);
		outp(0x03c9, g);
		outp(0x03c9, b);
	}
}

uint8_t* platform_get_framebuffer(void)
{
	return platform.pixels;
}

void platform_present(void)
{
}

void platform_uninit(void)
{
	video_mode_set_text();
}
