	.global _kbint
	.global _kbint_start
	.global _kbint_end

	.global _kbint_scancode

	.global _kbint_callback

	.text

_kbint_start:

_kbint_scancode: .byte 0

_kbint:
	mov $0x60, %dx # read port 0x60
	inb %dx, %al # store port input in %al

	mov %al, _kbint_scancode # write port input to _kbint_scancode_down

	call _kbint_callback
	ret
_kbint_end:
