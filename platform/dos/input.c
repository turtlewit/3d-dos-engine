#include <dos.h>
#include <bios.h>
#include <dpmi.h>
#include <go32.h>
#include <stdio.h>
#include <string.h>
#include <keys.h>
#include <stdlib.h>

#include "io.h"
#include "gameport.h"
#include "platform/input.h"
#include "platform/keys.h"
#include "scancode.h"

#define MAX_INPUT_EVENTS 128

extern void kbint(void);

extern uint8_t kbint_start;
extern uint8_t kbint_end;

extern uint8_t kbint_scancode;

_go32_dpmi_seginfo old_handler, new_handler;

__dpmi_meminfo locked_region;

typedef struct InputContext {
	InputEvent event_queue[MAX_INPUT_EVENTS];
	uint8_t event_queue_size : 5;
	JoystickValues js;

	uint8_t kbint_queue[MAX_INPUT_EVENTS];
	uint8_t kbint_queue_size;

	uint8_t modifiers;
	uint8_t last_key;
} InputContext;

static InputContext ctx;

bool input_is_char_available(void)
{
	return ctx.kbint_queue_size > 0;
}

static inline InputEvent* input_new_event(void)
{
	InputEvent* e = &ctx.event_queue[ctx.event_queue_size++];
	memset(e, 0, sizeof(InputEvent));
	return e;
}

static inline bool mouse_reset(uint16_t* buttons)
{
	union REGS registers;
	registers.x.ax = 0x00;
	int86(0x33, &registers, &registers);
	if (buttons) {
		*buttons = registers.x.bx;
	}
	return registers.x.ax > 0;
}

static inline void mouse_show_cursor(void)
{
	union REGS registers;
	registers.x.ax = 0x01;
	int86(0x33, &registers, &registers);
}

static inline void mouse_hide_cursor(void)
{
	union REGS registers;
	registers.x.ax = 0x02;
	int86(0x33, &registers, &registers);
}

extern void kbint_callback(void)
{
	if (kbint_scancode > 0 && ctx.kbint_queue_size < MAX_INPUT_EVENTS) {
		ctx.kbint_queue[ctx.kbint_queue_size++] = kbint_scancode;
		kbint_scancode = 0;
	}
}

void input_init(void)
{
	locked_region.size = &kbint_end - &kbint_start;
	locked_region.address = (unsigned long) kbint;
	__dpmi_lock_linear_region(&locked_region);

	_go32_dpmi_get_protected_mode_interrupt_vector(9, &old_handler);
	new_handler.pm_offset = (int) kbint;
	new_handler.pm_selector = _go32_my_cs();

	_go32_dpmi_chain_protected_mode_interrupt_vector(9, &new_handler);

	if (mouse_reset(NULL)) {
	}
}

bool input_poll(InputEvent* event) 
{
	JoystickValues js = read_joystick();
	for (uint8_t j = 0; j < 2; ++j) {
		for (uint8_t b = 0; b < 2; ++b) {
#define BUTTON_VALUE(js, j, b) (js.buttons & ((j << 1) + b))
			uint8_t button = BUTTON_VALUE(js, j, b);
			uint8_t last_button = BUTTON_VALUE(ctx.js, j, b);
#undef BUTTON_VALUE
			if (button != last_button) {
				InputEvent* e = input_new_event();
				e->type = INPUT_EVENT_JOYSTICK_BUTTON;
				e->joystick_button.joystick = j;
				e->joystick_button.button = b;
				e->joystick_button.pressed = button == 0;
			}
		}

		for (uint8_t a = 0; a < 2; ++a) {
#define AXIS_VALUE(js, j, a) (js.axis[(j << 1) + a])
			uint16_t axis = AXIS_VALUE(js, j, a);
			uint16_t last_axis = AXIS_VALUE(ctx.js, j, a);
#undef AXIS_IDX
			if ((axis == 128 && axis == last_axis) || axis == 0) {
				// don't send repeat 0 axis events
				continue;
			}

			int16_t nvalue = ((int16_t) axis) - 128;
			InputEvent* e = input_new_event();
			e->type = INPUT_EVENT_JOYSTICK_AXIS;
			e->joystick_axis.value = nvalue;
			e->joystick_axis.joystick = j;
			e->joystick_axis.axis = a;
		}
	}
	ctx.js = js;

	for (uint8_t i = 0; i < ctx.kbint_queue_size; ++i) {
		uint8_t scancode = ctx.kbint_queue[i];

		if (scancode > 0) {
			uint8_t key = scancode & ~0x80;
			bool pressed = key == scancode;

			if (!pressed) {
				ctx.last_key = 0;
			}

			if (ctx.last_key == key) {
				// ignore repeat keys
				continue;
			} else if (pressed) {
				ctx.last_key = key;
			}

			InputEvent* e = input_new_event();

			switch (scancode) {
				case KEY_RSHIFT:
				case KEY_LSHIFT: {
					e->type = INPUT_EVENT_KEYBOARD_MODIFIER;
					e->keyboard_modifier.modifier = INPUT_KEYBOARD_MODIFIER_SHIFT;
				} break;
				case KEY_ALT: {
					e->type = INPUT_EVENT_KEYBOARD_MODIFIER;
					e->keyboard_modifier.modifier = INPUT_KEYBOARD_MODIFIER_ALT;
				} break;
				case KEY_CTRL: {
					e->type = INPUT_EVENT_KEYBOARD_MODIFIER;
					e->keyboard_modifier.modifier = INPUT_KEYBOARD_MODIFIER_CTRL;
				} break;
				default: break;
			}

			if (e->type == INPUT_EVENT_KEYBOARD_MODIFIER) {
				e->keyboard_modifier.pressed = pressed;
				if (pressed) {
					ctx.modifiers &= ~e->keyboard_modifier.modifier;
				} else {
					ctx.modifiers |= e->keyboard_modifier.modifier;
				}
			} else {
				e->type = INPUT_EVENT_KEYBOARD;
				e->keyboard.modifiers = ctx.modifiers;
				e->keyboard.key = key;
				e->keyboard.pressed = pressed;
			}
		}
	}
	ctx.kbint_queue_size = 0;

	return input_pop_event(event);
}

bool input_pop_event(InputEvent* event) 
{
	if (ctx.event_queue_size == 0) {
		return false;
	}

	*event = ctx.event_queue[--ctx.event_queue_size];

	input_update_state(event);

	return true;
}

void input_uninit(void)
{
	mouse_reset(NULL);
	_go32_dpmi_set_protected_mode_interrupt_vector(9, &old_handler);
	__dpmi_unlock_linear_region(&locked_region);
}
