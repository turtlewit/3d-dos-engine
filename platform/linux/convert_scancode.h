#ifndef CONVERT_SCANCODE_H
#define CONVERT_SCANCODE_H

#include <stdint.h>
#include <SDL_scancode.h>
#include "platform/keys.h"

inline uint8_t convert_sdl_scancode(SDL_Scancode sdl_scancode)
{
	switch (sdl_scancode) {
		case SDL_SCANCODE_0: return KEY_0; break;
		case SDL_SCANCODE_1: return KEY_1; break;
		case SDL_SCANCODE_2: return KEY_2; break;
		case SDL_SCANCODE_3: return KEY_3; break;
		case SDL_SCANCODE_4: return KEY_4; break;
		case SDL_SCANCODE_5: return KEY_5; break;
		case SDL_SCANCODE_6: return KEY_6; break;
		case SDL_SCANCODE_7: return KEY_7; break;
		case SDL_SCANCODE_8: return KEY_8; break;
		case SDL_SCANCODE_9: return KEY_9; break;
		case SDL_SCANCODE_A: return KEY_A; break;
		case SDL_SCANCODE_B: return KEY_B; break;
		case SDL_SCANCODE_C: return KEY_C; break;
		case SDL_SCANCODE_D: return KEY_D; break;
		case SDL_SCANCODE_E: return KEY_E; break;
		case SDL_SCANCODE_F: return KEY_F; break;
		case SDL_SCANCODE_G: return KEY_G; break;
		case SDL_SCANCODE_H: return KEY_H; break;
		case SDL_SCANCODE_I: return KEY_I; break;
		case SDL_SCANCODE_J: return KEY_J; break;
		case SDL_SCANCODE_K: return KEY_K; break;
		case SDL_SCANCODE_L: return KEY_L; break;
		case SDL_SCANCODE_M: return KEY_M; break;
		case SDL_SCANCODE_N: return KEY_N; break;
		case SDL_SCANCODE_O: return KEY_O; break;
		case SDL_SCANCODE_P: return KEY_P; break;
		case SDL_SCANCODE_Q: return KEY_Q; break;
		case SDL_SCANCODE_R: return KEY_R; break;
		case SDL_SCANCODE_S: return KEY_S; break;
		case SDL_SCANCODE_T: return KEY_T; break;
		case SDL_SCANCODE_U: return KEY_U; break;
		case SDL_SCANCODE_V: return KEY_V; break;
		case SDL_SCANCODE_W: return KEY_W; break;
		case SDL_SCANCODE_X: return KEY_X; break;
		case SDL_SCANCODE_Y: return KEY_Y; break;
		case SDL_SCANCODE_Z: return KEY_Z; break;

		case SDL_SCANCODE_F1: return KEY_F1; break;
		case SDL_SCANCODE_F2: return KEY_F2; break;
		case SDL_SCANCODE_F3: return KEY_F3; break;
		case SDL_SCANCODE_F4: return KEY_F4; break;
		case SDL_SCANCODE_F5: return KEY_F5; break;
		case SDL_SCANCODE_F6: return KEY_F6; break;
		case SDL_SCANCODE_F7: return KEY_F7; break;
		case SDL_SCANCODE_F8: return KEY_F8; break;
		case SDL_SCANCODE_F9: return KEY_F9; break;
		case SDL_SCANCODE_F10: return KEY_F10; break;
		case SDL_SCANCODE_F11: return KEY_F11; break;
		case SDL_SCANCODE_F12: return KEY_F12; break;

		case SDL_SCANCODE_KP_0: return KEY_KP0; break;
		case SDL_SCANCODE_KP_1: return KEY_KP1; break;
		case SDL_SCANCODE_KP_2: return KEY_KP2; break;
		case SDL_SCANCODE_KP_3: return KEY_KP3; break;
		case SDL_SCANCODE_KP_4: return KEY_KP4; break;
		case SDL_SCANCODE_KP_5: return KEY_KP5; break;
		case SDL_SCANCODE_KP_6: return KEY_KP6; break;
		case SDL_SCANCODE_KP_7: return KEY_KP7; break;
		case SDL_SCANCODE_KP_8: return KEY_KP8; break;
		case SDL_SCANCODE_KP_9: return KEY_KP9; break;
		case SDL_SCANCODE_KP_MINUS: return KEY_KPDASH; break;
		case SDL_SCANCODE_KP_PERIOD: return KEY_KPDOT; break;
		case SDL_SCANCODE_KP_DIVIDE: return KEY_SLASH; break;
		case SDL_SCANCODE_KP_PLUS: return KEY_PLUS; break;
		case SDL_SCANCODE_KP_MULTIPLY: return KEY_STAR; break;
		case SDL_SCANCODE_KP_ENTER: return KEY_ENTER; break;

		case SDL_SCANCODE_UP: return KEY_UP; break;
		case SDL_SCANCODE_DOWN: return KEY_DOWN; break;
		case SDL_SCANCODE_LEFT: return KEY_LEFT; break;
		case SDL_SCANCODE_RIGHT: return KEY_RIGHT; break;

		case SDL_SCANCODE_LCTRL: return KEY_CTRL; break;
		case SDL_SCANCODE_LALT: return KEY_ALT; break;
		case SDL_SCANCODE_RCTRL: return KEY_CTRL; break;
		case SDL_SCANCODE_RALT: return KEY_ALT; break;
		case SDL_SCANCODE_LSHIFT: return KEY_LSHIFT; break;
		case SDL_SCANCODE_RSHIFT: return KEY_RSHIFT; break;

		case SDL_SCANCODE_APOSTROPHE: return KEY_APOSTROPHE; break;
		case SDL_SCANCODE_BACKSLASH: return KEY_BACKSLASH; break;
		case SDL_SCANCODE_BACKSPACE: return KEY_BACKSPACE; break;
		case SDL_SCANCODE_CAPSLOCK: return KEY_CAPSLOCK; break;
		case SDL_SCANCODE_COMMA: return KEY_COMMA; break;
		case SDL_SCANCODE_MINUS: return KEY_DASH; break;
		case SDL_SCANCODE_DELETE: return KEY_DEL; break;
		case SDL_SCANCODE_PERIOD: return KEY_DOT; break;
		case SDL_SCANCODE_END: return KEY_END; break;
		case SDL_SCANCODE_RETURN: return KEY_ENTER; break;
		case SDL_SCANCODE_EQUALS: return KEY_EQUALS; break;
		case SDL_SCANCODE_ESCAPE: return KEY_ESC; break;
		case SDL_SCANCODE_GRAVE: return KEY_GRAVE; break;
		case SDL_SCANCODE_HOME: return KEY_HOME; break;
		case SDL_SCANCODE_INSERT: return KEY_INSERT; break;
		case SDL_SCANCODE_LEFTBRACKET: return KEY_LBRACKET; break;
		//case SDL_SCANCODE_META: return KEY_META; break;
		case SDL_SCANCODE_NUMLOCKCLEAR: return KEY_NUMLOCK; break;
		case SDL_SCANCODE_PAGEDOWN: return KEY_PAGEDOWN; break;
		case SDL_SCANCODE_PAGEUP: return KEY_PAGEUP; break;
		case SDL_SCANCODE_PAUSE: return KEY_PAUSE; break;
		//case SDL_SCANCODE_PLUS: return KEY_PLUS; break;
		case SDL_SCANCODE_RIGHTBRACKET: return KEY_RBRACKET; break;
		case SDL_SCANCODE_SCROLLLOCK: return KEY_SCRLK; break;
		case SDL_SCANCODE_SEMICOLON: return KEY_SEMICOLON; break;
		case SDL_SCANCODE_SLASH: return KEY_SLASH; break;
		case SDL_SCANCODE_SPACE: return KEY_SPACE; break;
		//case SDL_SCANCODE_STAR: return KEY_STAR; break;
		case SDL_SCANCODE_TAB: return KEY_TAB; break;

		default: break;
	}
	return 0;
};

#endif
