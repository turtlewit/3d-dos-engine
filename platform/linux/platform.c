#include <SDL.h>
#include <SDL_video.h>
#include <SDL_surface.h>
#include <SDL_pixels.h>
#include <SDL_log.h>

#include "platform/platform.h"
#include "platform/input.h"
#include "core/mainloop.h"
#include "convert_scancode.h"

typedef struct LinuxPlatform {
	SDL_Window* window;
	SDL_Surface* window_surface;
	SDL_Surface* framebuffer_surface;
	SDL_Surface* conversion_surface;
	SDL_Palette* palette;
} LinuxPlatform;

LinuxPlatform linux_platform;

void platform_init(void)
{
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER);

	linux_platform.window = SDL_CreateWindow(
		"game",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		1600, 1200,
		0);

	linux_platform.window_surface = SDL_GetWindowSurface(linux_platform.window);

	linux_platform.framebuffer_surface = SDL_CreateRGBSurfaceWithFormat(
		0,
		320, 200,
		8,
		SDL_PIXELFORMAT_INDEX8);
	SDL_LockSurface(linux_platform.framebuffer_surface);

	linux_platform.conversion_surface = SDL_CreateRGBSurfaceWithFormat(
		0,
		320, 200,
		24,
		SDL_PIXELFORMAT_RGB24);

	linux_platform.palette = SDL_AllocPalette(256);

	SDL_LogSetAllPriority(SDL_LOG_PRIORITY_WARN);

	//SDL_FillRect(linux_platform.window_surface, NULL, SDL_MapRGBA(linux_platform.window_surface->format, 255, 255, 255, 255));
	SDL_UpdateWindowSurface(linux_platform.window);
}

void platform_set_palette(uint8_t* palette)
{
	SDL_Color colors[256];

	for (uint32_t i = 0; i < 256; ++i) {
		const uint32_t idx = i * 3;
		colors[i].r = palette[idx];
		colors[i].g = palette[idx+1];
		colors[i].b = palette[idx+2];
		colors[i].a = 255;
	}

	SDL_SetPaletteColors(
		linux_platform.palette,
		colors,
		0, 256);

	SDL_SetSurfacePalette(
		linux_platform.framebuffer_surface,
		linux_platform.palette);
}

uint8_t* platform_get_framebuffer(void)
{
	return (uint8_t*)linux_platform.framebuffer_surface->pixels;
}

void platform_present(void)
{
	SDL_UnlockSurface(linux_platform.framebuffer_surface);
	SDL_BlitSurface(
		linux_platform.framebuffer_surface,
		NULL,
		linux_platform.conversion_surface,
		NULL);
	SDL_BlitScaled(
		linux_platform.conversion_surface,
		NULL,
		linux_platform.window_surface,
		NULL);
	SDL_UpdateWindowSurface(linux_platform.window);
	SDL_LockSurface(linux_platform.framebuffer_surface);
}

void platform_uninit(void)
{
	SDL_FreeSurface(linux_platform.framebuffer_surface);
	linux_platform.framebuffer_surface = NULL;

	SDL_FreePalette(linux_platform.palette);
	linux_platform.palette = NULL;

	SDL_DestroyWindowSurface(linux_platform.window);
	linux_platform.window_surface = NULL;

	SDL_DestroyWindow(linux_platform.window);
	linux_platform.window = NULL;

	SDL_Quit();
}

static bool is_keycode_mod(SDL_Keycode keycode)
{
	switch (keycode) {
		case SDLK_LSHIFT:
		case SDLK_RSHIFT:
		case SDLK_LALT:
		case SDLK_RALT:
		case SDLK_LCTRL:
		case SDLK_RCTRL:
			return true;
		default: break;
	}
	return false;
}

static bool convert_sdl_event_type(const SDL_Event* sdl_event, InputEventType* out_event_type)
{
	switch (sdl_event->type) {
		case SDL_KEYUP:
		case SDL_KEYDOWN: {
			if (is_keycode_mod(sdl_event->key.keysym.sym)) {
				*out_event_type = INPUT_EVENT_KEYBOARD_MODIFIER;
			} else {
				*out_event_type = INPUT_EVENT_KEYBOARD;
			}
		} break;

		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
		case SDL_MOUSEWHEEL:
			*out_event_type = INPUT_EVENT_MOUSE_BUTTON;
			break;

		case SDL_MOUSEMOTION:
			*out_event_type = INPUT_EVENT_MOUSE_MOTION;
			break;

		case SDL_CONTROLLERBUTTONDOWN:
		case SDL_CONTROLLERBUTTONUP:
			*out_event_type = INPUT_EVENT_JOYSTICK_BUTTON;
			break;

		case SDL_CONTROLLERAXISMOTION:
			*out_event_type = INPUT_EVENT_JOYSTICK_AXIS;
			break;

		default:
			return false;
	}


	return true;
}

static uint8_t get_modifiers(void)
{
	uint8_t modifiers = 0;
	SDL_Keymod keymod = SDL_GetModState();

	if (keymod & (KMOD_LSHIFT | KMOD_RSHIFT)) {
		modifiers |= INPUT_KEYBOARD_MODIFIER_SHIFT;
	}

	if (keymod & (KMOD_LCTRL | KMOD_RCTRL)) {
		modifiers |= INPUT_KEYBOARD_MODIFIER_CTRL;
	}

	if (keymod & (KMOD_LALT | KMOD_RALT)) {
		modifiers |= INPUT_KEYBOARD_MODIFIER_ALT;
	}

	return modifiers;
}

static uint8_t convert_sdl_modifier(SDL_Keycode code)
{
	switch (code) {
		case SDLK_LSHIFT:
		case SDLK_RSHIFT: {
			return INPUT_KEYBOARD_MODIFIER_SHIFT;
		} break;

		case SDLK_LALT:
		case SDLK_RALT: {
			return INPUT_KEYBOARD_MODIFIER_ALT;
		} break;

		case SDLK_LCTRL:
		case SDLK_RCTRL: {
			return INPUT_KEYBOARD_MODIFIER_CTRL;
		} break;

		default: break;
	}

	return 0;
}

static bool convert_sdl_event(const SDL_Event* sdl_event, InputEvent* out_event)
{
	bool result = convert_sdl_event_type(sdl_event, &out_event->type);

	if (result) {
		if (sdl_event->key.repeat) {
			return false;
		}

		switch (out_event->type) {
			case INPUT_EVENT_KEYBOARD: {
				out_event->keyboard.key = convert_sdl_scancode(sdl_event->key.keysym.scancode);
				out_event->keyboard.modifiers = get_modifiers();
				out_event->keyboard.pressed = sdl_event->key.type == SDL_KEYDOWN ? true : false;
			} break;

			case INPUT_EVENT_KEYBOARD_MODIFIER: {
				out_event->keyboard_modifier.modifier = convert_sdl_modifier(sdl_event->key.keysym.sym);
				out_event->keyboard_modifier.pressed = sdl_event->key.type == SDL_KEYDOWN ? true : false;
			} break;

			case INPUT_EVENT_MOUSE_BUTTON: {
				out_event->mouse_button.button = SDL_BUTTON(sdl_event->button.button);
				out_event->mouse_button.mask = SDL_GetMouseState(NULL, NULL);
				out_event->mouse_button.pressed = sdl_event->button.type == SDL_MOUSEBUTTONDOWN ? true : false;
			} break;

			case INPUT_EVENT_MOUSE_MOTION: {
				if (input_get_mouse_warp()) {
					out_event->mouse_motion.x = sdl_event->motion.xrel;
					out_event->mouse_motion.y = sdl_event->motion.yrel;
				} else {
					out_event->mouse_motion.x = sdl_event->motion.x;
					out_event->mouse_motion.y = sdl_event->motion.y;
				}
			} break;

			case INPUT_EVENT_JOYSTICK_BUTTON: {
				uint8_t sdl_button = sdl_event->cbutton.button;
				if (sdl_button != SDL_CONTROLLER_BUTTON_A || sdl_button != SDL_CONTROLLER_BUTTON_B) {
					return false;
				};

				if (sdl_event->cbutton.which > 1 || sdl_event->cbutton.which < 0) {
					return false;
				}

				out_event->joystick_button.joystick = sdl_event->cbutton.which;
				out_event->joystick_button.button = sdl_button == SDL_CONTROLLER_BUTTON_A ? 0 : 1;
				out_event->joystick_button.pressed = sdl_event->cbutton.type == SDL_CONTROLLERBUTTONDOWN ? true : false;
			} break;

			case INPUT_EVENT_JOYSTICK_AXIS: {
				if (sdl_event->caxis.which > 1 || sdl_event->caxis.which < 0) {
					return false;
				}
				if (sdl_event->caxis.axis > 1 || sdl_event->caxis.axis < 0) {
					return false;
				}

				out_event->joystick_axis.value = sdl_event->caxis.value;
				out_event->joystick_axis.joystick = sdl_event->caxis.which;
				out_event->joystick_axis.axis = sdl_event->caxis.axis;
			} break;

			default: break;
		}
	} else {
		if (sdl_event->type == SDL_QUIT) {
			quit();
		}
	}

	return result;
}

void input_init(void)
{
}

bool is_input_char_available(void)
{
	return false;
}

bool input_poll(InputEvent* event)
{
	SDL_Event sdl_event;
	bool poll_result = true;
	bool convert_result = false;

	while (!convert_result) {
		poll_result = SDL_PollEvent(&sdl_event);

		if (!poll_result) {
			break;
		}

		convert_result = convert_sdl_event(&sdl_event, event);
	}

	return poll_result;
}

bool input_pop_event(InputEvent* event)
{
	return false;
}

void input_uninit(void)
{
}

int main(int argc, char** argv)
{
	return mainloop(argc, argv);
}
