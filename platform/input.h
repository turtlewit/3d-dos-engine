#ifndef INPUT_H
#define INPUT_H

#include <stdbool.h>
#include <stdint.h>

// bitfields are a size-to-performance tradeoff
// but they're still nice for compiler warnings
#define PAD_BITFIELDS
#ifdef PAD_BITFIELDS
# define BITFIELD_PAD(t, n) ; t : n
#else
# define BITFIELD_PAD(t, n)
#endif


/* Keyboard */

typedef enum InputKeyboardModifiers {
	INPUT_KEYBOARD_MODIFIER_SHIFT = 1,
	INPUT_KEYBOARD_MODIFIER_CTRL  = 1 << 1,
	INPUT_KEYBOARD_MODIFIER_ALT   = 1 << 2,
} InputKeyboardModifiers;

// key will never be a "modifier" key.
// modifier update events are sent with
// key = 0, and the modifier = only the pressed
// or released modifier
typedef struct InputEventKeyboard {
	uint8_t key;
	uint8_t modifiers : 3 BITFIELD_PAD(uint8_t, 5);
	bool    pressed   : 1 BITFIELD_PAD(uint8_t, 7);
} InputEventKeyboard;

typedef struct InputEventKeyboardModifier {
	uint8_t modifier : 3 BITFIELD_PAD(uint8_t, 5);
	bool pressed     : 1 BITFIELD_PAD(uint8_t, 7);
} InputEventKeyboardModifier;


/* Mouse */

typedef enum InputMouseButtonMask {
	INPUT_MOUSE_BUTTON_MASK_LEFT   = 1,
	INPUT_MOUSE_BUTTON_MASK_MIDDLE = 1 << 1,
	INPUT_MOUSE_BUTTON_MASK_RIGHT  = 1 << 2,
} InputMouseButtonMask;

typedef struct InputEventMouseButton {
	InputMouseButtonMask button  : 3 BITFIELD_PAD(InputMouseButtonMask, 5);
	InputMouseButtonMask mask    : 3 BITFIELD_PAD(InputMouseButtonMask, 5);
	bool                 pressed : 1 BITFIELD_PAD(uint8_t, 7);
} InputEventMouseButton;

// x and y are absolute when mouse warp is disabled,
// and are relative when mouse warp is enabled
typedef struct InputEventMouseMotion {
	uint16_t x, y;
} InputEventMouseMotion;


/* Joystick */

typedef struct InputEventJoystickButton {
	uint8_t joystick : 1 BITFIELD_PAD(uint8_t, 7);
	uint8_t button   : 1 BITFIELD_PAD(uint8_t, 7); /* only 2 buttons */
	bool    pressed  : 1 BITFIELD_PAD(uint8_t, 7);
} InputEventJoystickButton;

typedef enum InputJoystickAxis {
	INPUT_JOYSTICK_AXIS_HORIZONTAL,
	INPUT_JOYSTICK_AXIS_VERTICAL,
} InputJoystickAxis;

typedef struct InputEventJoystickAxis {
	int16_t           value;
	uint8_t           joystick : 1 BITFIELD_PAD(uint8_t, 7); /* only 2 joysticks */
	InputJoystickAxis axis     : 1 BITFIELD_PAD(InputJoystickAxis, 7);
} InputEventJoystickAxis;


/* Event */

typedef enum InputEventType {
	INPUT_EVENT_KEYBOARD,
	INPUT_EVENT_KEYBOARD_MODIFIER,
	INPUT_EVENT_MOUSE_BUTTON,
	INPUT_EVENT_MOUSE_MOTION,
	INPUT_EVENT_JOYSTICK_BUTTON,
	INPUT_EVENT_JOYSTICK_AXIS,

	INPUT_EVENT_LAST,
	INPUT_EVENT_NUM = INPUT_EVENT_LAST - 1,
} InputEventType;

typedef struct InputEvent {
	InputEventType type;
	union {
		InputEventKeyboard keyboard;
		InputEventKeyboardModifier keyboard_modifier;
		InputEventMouseButton mouse_button;
		InputEventMouseMotion mouse_motion;
		InputEventJoystickButton joystick_button;
		InputEventJoystickAxis joystick_axis;
	};
} InputEvent;


/* Plaform-specific (platform/[platform]/input.c) */

void input_init(void);

bool input_is_char_available(void);

bool input_poll(InputEvent* event);
bool input_pop_event(InputEvent* event);

void input_uninit(void);


/* Platform-agnostic (platform/input.c) */

void input_update_state(InputEvent* event);

bool input_is_joystick_button_pressed(uint8_t joystick, uint8_t button);
int16_t input_get_joystick_axis(uint8_t joystick, uint8_t axis);

uint8_t input_get_pressed_key(void);
inline bool input_is_key_pressed(uint8_t key) { return key == input_get_pressed_key(); }
uint8_t input_get_keyboard_modifiers(void);
inline bool input_are_keyboard_modifiers_pressed(uint8_t modifiers) { return (input_get_keyboard_modifiers() & modifiers) > 0; }

void input_get_mouse_position(uint16_t* x, uint16_t* y);
bool input_get_mouse_visible(void);
void input_set_mouse_visible(bool visible);
bool input_get_mouse_warp(void);
void input_set_mouse_warp(bool warp);
uint8_t input_get_mouse_button_mask(void);

#endif
