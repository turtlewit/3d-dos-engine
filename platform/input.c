#include "input.h"

typedef struct Joystick {
	bool button_pressed[2];
	int16_t axis_value[2];
} Joystick;

typedef struct BaseInputContext {
	Joystick joystick[2];
	uint8_t pressed_key;
	uint8_t modifiers;
	uint16_t mouse_position[2];
	bool mouse_visible;
	bool mouse_warp;
	uint8_t mouse_button_mask;
} BaseInputContext;

static BaseInputContext ctx;

void input_update_state(InputEvent* event)
{
	switch (event->type) {

	case INPUT_EVENT_KEYBOARD: {
		if (event->keyboard.pressed) {
			ctx.pressed_key = event->keyboard.key;
		} else {
			ctx.pressed_key = 0;
		}
		ctx.modifiers = event->keyboard.modifiers;
	} break;

	case INPUT_EVENT_KEYBOARD_MODIFIER: {
		uint8_t mask = event->keyboard_modifier.modifier;
		if (!event->keyboard_modifier.pressed) {
			ctx.modifiers &= ~mask;
		} else {
			ctx.modifiers |= mask;
		}
	} break;

	case INPUT_EVENT_MOUSE_BUTTON: {
		ctx.mouse_button_mask = event->mouse_button.mask;
	} break;

	case INPUT_EVENT_MOUSE_MOTION: {
		if (!ctx.mouse_warp) {
			ctx.mouse_position[0] = event->mouse_motion.x;
			ctx.mouse_position[1] = event->mouse_motion.y;
		}
	} break;

	case INPUT_EVENT_JOYSTICK_BUTTON: {
		ctx.joystick[event->joystick_button.joystick].button_pressed[event->joystick_button.button] = event->joystick_button.pressed;
	} break;

	case INPUT_EVENT_JOYSTICK_AXIS: {
		ctx.joystick[event->joystick_axis.joystick].axis_value[event->joystick_axis.axis] = event->joystick_axis.value;
	} break;

	default: break;

	}
}

bool input_is_joystick_button_pressed(uint8_t joystick, uint8_t button)
{
	return ctx.joystick[joystick].button_pressed[button];
}

int16_t input_get_joystick_axis(uint8_t joystick, uint8_t axis)
{
	return ctx.joystick[joystick].axis_value[axis];
}

uint8_t input_get_pressed_key(void)
{
	return ctx.pressed_key;
}

uint8_t input_get_keyboard_modifiers(void)
{
	return ctx.modifiers;
}

void input_get_mouse_position(uint16_t* x, uint16_t* y)
{
	*x = ctx.mouse_position[0];
	*y = ctx.mouse_position[1];
}

bool input_get_mouse_visible(void)
{
	return ctx.mouse_visible;
}

void input_set_mouse_visible(bool visible)
{
	ctx.mouse_visible = visible;
}

bool input_get_mouse_warp(void)
{
	return ctx.mouse_warp;
}

void input_set_mouse_warp(bool warp)
{
	ctx.mouse_warp = warp;
}

uint8_t input_get_mouse_button_mask(void)
{
	return ctx.mouse_button_mask;
}
