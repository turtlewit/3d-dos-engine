#ifndef PLATFORM_H
#define PLATFORM_H

#include <stdint.h>

void platform_init(void);
void platform_set_palette(uint8_t* palette);
uint8_t* platform_get_framebuffer(void);
void platform_present(void);
void platform_uninit(void);

#endif
