#ifndef PALETTE_H
#define PALETTE_H

#include <stdlib.h>
#include <stdint.h>

#include "platform/platform.h"

typedef struct Palette {
	uint8_t* data;
} Palette;

inline void palette_init(Palette* palette, uint8_t* data)
{
	palette->data = data;
}

inline void palette_set_active(Palette* palette)
{
	platform_set_palette(palette->data);
}

inline void palette_uninit(Palette* palette)
{
	if (palette && palette->data) {
		free(palette->data);
		palette->data = NULL;
	}
}

#endif
