#ifndef MESH_INSTANCE_H
#define MESH_INSTANCE_H

#include "mesh.h"
#include "image.h"

typedef struct MeshInstance MeshInstance;

MeshInstance* mesh_instance_init(Mesh* mesh, Image* texture);

mat4* mesh_instance_get_matrix(MeshInstance* mesh_instance);
void mesh_instance_set_matrix(MeshInstance* mesh_instance, mat4* matrix);

void mesh_instance_uninit(MeshInstance* mesh_instance);

#endif
