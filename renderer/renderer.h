#ifndef RENDERER_H
#define RENDERER_H

#include <stdint.h>
#include <cglm/cglm.h>

mat4* rd_get_view();
void rd_set_view(mat4* view);

mat4* rd_get_proj();
void rd_set_proj(mat4* proj);

void rd_clear(void);
void rd_render(void);
void rd_present(void);

#endif
