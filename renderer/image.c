#include "image.h"

extern inline void image_from_raw(Image* image, uint8_t* data, uint32_t width, uint32_t height);
extern inline void image_uninit(Image* image);
