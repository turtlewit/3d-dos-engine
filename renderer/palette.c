#include "palette.h"

extern inline void palette_init(Palette* palette, uint8_t* data);
extern inline void palette_set_active(Palette* palette);
extern inline void palette_uninit(Palette* palette);
