#ifndef MESH_H
#define MESH_H

#include <cglm/cglm.h>

typedef struct Vertex {
	float u,v;
	vec3 pos;
} Vertex;

typedef struct Mesh Mesh;

Mesh* mesh_new();
void mesh_init(Mesh* mesh, Vertex* vertices, uint32_t vertex_count);

void mesh_uninit(Mesh* mesh);

#endif
