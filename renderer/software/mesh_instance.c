#include "mesh_instance.h"
#include "renderer.h"

#include "core/list.h"

MeshInstance* mesh_instance_init(Mesh* mesh, Image* texture)
{
	MeshInstance* mesh_instance = malloc(sizeof(MeshInstance));
	list_add(&rd.mesh_instances, mesh_instance);

	mesh_instance->mesh = mesh;
	mesh_instance->texture = texture;

	glm_mat4_identity(mesh_instance->matrix);

	return mesh_instance;
}

mat4* mesh_instance_get_matrix(MeshInstance* mesh_instance)
{
	return &mesh_instance->matrix;
}

void mesh_instance_set_matrix(MeshInstance* mesh_instance, mat4* matrix)
{
	glm_mat4_copy(*matrix, mesh_instance->matrix);
}

void mesh_instance_uninit(MeshInstance* mesh_instance)
{
	list_remove(&rd.mesh_instances, mesh_instance);
	free(mesh_instance);
}
