#ifndef SOFTWARE_MESH_H
#define SOFTWARE_MESH_H

#include <cglm/cglm.h>

#include "renderer/mesh.h"

struct Mesh {
	uint32_t vertex_count;
	Vertex* vertices;
};

#endif
