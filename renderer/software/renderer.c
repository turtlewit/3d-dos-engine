#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "renderer.h"

#include "platform/platform.h"
#include "core/util.h"

#define MAX_TRIS 4096

SoftwareRenderer rd;

void rd_present(void)
{
	uint8_t* pixels = platform_get_framebuffer();
	memcpy(pixels, rd.framebuffer, XRES * YRES);
	platform_present();
}

mat4* rd_get_view()
{
	return &rd.view;
}

void rd_set_view(mat4* view)
{
	glm_mat4_copy(*view, rd.view);
}

mat4* rd_get_proj()
{
	return &rd.proj;
}

void rd_set_proj(mat4* proj)
{
	glm_mat4_copy(*proj, rd.proj);
}

void rd_clear(void)
{
	memset(rd.framebuffer, 0xff, XRES * YRES);
}

//void rd_draw_image(uint8_t* data, uint32_t width, uint32_t height)
//{
//	uint16_t n = width * height;
//	for (uint16_t i = 0; i < n; ++i) {
//		uint16_t screen_index = ( ( i % width ) + ( ( i / width ) * XRES) );
//		rd.framebuffer[screen_index] = data[i];
//	}
//}

typedef struct Slopes {
	float x, u, v;
} Slopes;

static inline uint8_t shader(MeshInstance* mi, float u, float v)
{
	uint16_t x = u * (mi->texture->width - 1);
	x = clamp(x, 0, mi->texture->width - 1);
	uint16_t y = v * (mi->texture->height - 1);
	y = clamp(y, 0, mi->texture->height - 1);
	uint8_t result = mi->texture->data[COORD_TO_INDEX(x, y, mi->texture->width)];
	return result;
}

static inline void fill_flat_tri(MeshInstance* mi, ScreenVert vert, uint16_t maxy, Slopes ls, Slopes rs)
{
	float lx = vert.x;
	float lu = vert.u;
	float lv = vert.v;
	float rx = vert.x;
	float ru = vert.u;
	float rv = vert.v;

	int16_t dy;
	uint16_t vsteps;
	if (maxy > vert.y) {
		dy = 1;
		vsteps = (maxy - vert.y) + 1;
	} else {
		dy = -1;
		vsteps = (vert.y - maxy) + 1;
	}
	int16_t y = vert.y;
	for (uint16_t i = 0; i < vsteps; ++i) {
		float steps = rx - lx;
		float du = (ru - lu) / steps;
		float dv = (rv - lv) / steps;
		float u = lu;
		float v = lv;
		uint16_t index = y * XRES;
		for (uint16_t x = lx; x <= rx; ++x) {
			uint8_t color = shader(mi, u, v);
			rd.framebuffer[index + ((int16_t) x)] = color;
			u += du;
			v += dv;
		}
		lx += ls.x;
		lu += ls.u;
		lv += ls.v;
		rx += rs.x;
		ru += rs.u;
		rv += rs.v;
		y += dy;
	}
}

static inline bool front_facing(vec3 v, vec3 edges[2]) {
	vec3 n;
	vec3 negv;
	glm_vec3_cross(edges[0], edges[1], n);
	glm_vec3_scale(v, -1, negv);
	return glm_vec3_dot(negv, n) < 0;
}

#define RADIX_COMPARE(x, r) ((x) & (1 << (r)))

// radix sort
static void triangle_sort(ScreenTri** array, ScreenTri** end, uint16_t radix) {
	if (array + 1 >= end) {
		return;
	}

	ScreenTri** l = array;
	ScreenTri** r = end - 1;
	while (l < r) {
		for (; l < r && RADIX_COMPARE(l[0]->avg_z, radix) == 0; ++l) {
		}
		if (l >= r) {
			break;
		}

		for (; l < r && RADIX_COMPARE(r[0]->avg_z, radix) > 0; --r) {
		}
		if (l >= r) {
			break;
		}

		swap(*l, *r, ScreenTri*);
		++l;
		--r;
	}

	if (radix == 0) {
		return;
	}

	radix -= 1;

	if (RADIX_COMPARE(l[0]->avg_z, radix + 1) == 0) {
		triangle_sort(array, l + 1, radix);
		triangle_sort(l + 1, end, radix);
	} else {
		triangle_sort(array, l, radix);
		triangle_sort(l, end, radix);
	}
}

void rd_render(void)
{
	ScreenTri tris[4096];
	uint16_t current_tri = 0;

	mat4 vp;
	glm_mat4_mul(rd.proj, rd.view, vp);

	// triangle transformation

	for (MeshInstance* mi = rd.mesh_instances; mi; mi = mi->next) {
		mat4 mvp;
		glm_mat4_mul(vp, mi->matrix, mvp);

		for (uint16_t i = 0; i < mi->mesh->vertex_count; i += 3) {
			if (current_tri >= MAX_TRIS) {
				break;
			}
			Vertex* verts = &mi->mesh->vertices[i];
			ScreenTri* tri = &tris[current_tri++];
			tri->mesh_instance = mi;

			vec3 points[3];
			float ws[3];

			bool clip = false;
		       	for (uint8_t j = 0; j < 3; ++j) {
				vec4 pos;
				glm_vec4(verts[j].pos, 1.f, pos);
				glm_mat4_mulv(mvp, pos, pos);

				if (!in_range_exclusive(pos[0], -pos[3], pos[3]) 
						|| !in_range_exclusive(pos[1], -pos[3], pos[3]) 
						|| !in_range_exclusive(pos[2], -pos[3], pos[3])) {
					clip = true;
					break;
				}

				glm_vec3(pos, points[j]);
				ws[j] = pos[3];

				int16_t x = (((pos[0] / pos[3]) + 1) / 2) * XRES;
				int16_t y = (((pos[1] / pos[3]) + 1) / 2) * YRES;

				ScreenVert* v = &tri->verts[j];
				*v = (ScreenVert) {
					x,
					y,
					verts[j].u,
					verts[j].v,
				};
			}

			if (clip) {
				current_tri -= 1;
				continue;
			}

			vec3 edges[2];
			glm_vec3_sub(points[1], points[0], edges[0]);
			glm_vec3_sub(points[2], points[0], edges[1]);

			// cull back faces
			if (!front_facing(points[0], edges)) {
				current_tri -= 1;
			} else {
				float avg_z = ((points[0][2] / ws[0]) + (points[1][2] / ws[1]) + (points[2][2] / ws[2])) / 3.f;
				avg_z = (avg_z + 1.f) / 2.f;
				tri->avg_z = avg_z * UINT16_MAX;
			}
		}

		if (current_tri >= MAX_TRIS) {
			break;
		}
	}

	// sorting
	ScreenTri* sorted_tris[4096];
	for (uint16_t i = 0; i < current_tri; ++i) {
		sorted_tris[i] = tris + i;
	}

	triangle_sort(sorted_tris, sorted_tris + current_tri, 15);

	for (int16_t i = current_tri - 1; i >= 0; --i) {
		ScreenTri tri = *sorted_tris[i];
		uint8_t a = 0;
		uint8_t b = 1;
		uint8_t c = 2;

		//printf("y values: %d, %d, %d\n", tri.verts[a].y, tri.verts[b].y, tri.verts[c].y); 

		if (tri.verts[a].y > tri.verts[b].y) {
			swap(a, b, uint8_t);
		}

		if (tri.verts[b].y > tri.verts[c].y) {
			swap(b, c, uint8_t);

			if (tri.verts[a].y > tri.verts[b].y) {
				swap(a, b, uint8_t);
			}
		}

		//printf("y values: %d, %d, %d\n", tri.verts[a].y, tri.verts[b].y, tri.verts[c].y); 

		uint16_t maxy = tri.verts[b].y;

		float bsteps = maxy - tri.verts[a].y;
		float csteps = bsteps + (tri.verts[c].y - maxy);

		float lsteps, rsteps;

		uint8_t l = b;
		uint8_t r = c;

#define SLOPE(i, j, attr, steps) (((float) tri.verts[i].attr) - ((float) tri.verts[j].attr)) / steps

		//if (tri.verts[r].x < tri.verts[l].x) {
		if (SLOPE(r, a, x, csteps) < SLOPE(l, a, x, bsteps)) {
			swap(l, r, uint8_t);
			lsteps = csteps;
			rsteps = bsteps;
		} else {
			lsteps = bsteps;
			rsteps = csteps;
		}

		Slopes sl, sr;

		//printf("x values: %d, %d, %d\n", tri.verts[a].x, tri.verts[b].x, tri.verts[c].x); 


		sl.x = SLOPE(l, a, x, lsteps);
		sl.u = SLOPE(l, a, u, lsteps);
		sl.v = SLOPE(l, a, v, lsteps);
		sr.x = SLOPE(r, a, x, rsteps);
		sr.u = SLOPE(r, a, u, rsteps);
		sr.v = SLOPE(r, a, v, rsteps);

		//printf("l and r slopes: %f, %f\n", sl.x, sr.x);

		fill_flat_tri(tri.mesh_instance, tri.verts[a], maxy, sl, sr);

		l = a;
		r = b;

		bsteps = csteps - bsteps;

		//if (tri.verts[r].y < tri.verts[l].y) {
		if (SLOPE(l, c, x, csteps) > SLOPE(r, c, x, bsteps)) {
			swap(l, r, uint8_t);
			lsteps = bsteps;
			rsteps = csteps;
		} else {
			lsteps = csteps;
			rsteps = bsteps;
		}

		sl.x = SLOPE(l, c, x, lsteps);
		sl.u = SLOPE(l, c, u, lsteps);
		sl.v = SLOPE(l, c, v, lsteps);
		sr.x = SLOPE(r, c, x, rsteps);
		sr.u = SLOPE(r, c, u, rsteps);
		sr.v = SLOPE(r, c, v, rsteps);

		fill_flat_tri(tri.mesh_instance, tri.verts[c], maxy, sl, sr);

#undef SLOPE

	}
}
