#include "mesh.h"
#include "renderer.h"

Mesh* mesh_new()
{
	return malloc(sizeof(Mesh));
}

void mesh_init(Mesh* mesh, Vertex* vertices, uint32_t vertex_count)
{
	mesh->vertex_count = vertex_count;
	mesh->vertices = vertices;
}

void mesh_uninit(Mesh* mesh)
{
	if (mesh) {
		if (mesh->vertices) {
			free(mesh->vertices);
		}
		free(mesh);
	}
}
