#ifndef SOFTWARE_MESH_INSTANCE_H
#define SOFTWARE_MESH_INSTANCE_H

#include <cglm/cglm.h>

#include "renderer/mesh_instance.h"
#include "mesh.h"
#include "renderer/image.h"

struct MeshInstance {
	struct MeshInstance* next;
	struct MeshInstance* prev;
	Mesh* mesh;
	Image* texture;
	
	// TODO: optimization: try 3x4 matrix
	mat4 matrix;
};

#endif
