#ifndef SOFTWARE_RENDERER_H
#define SOFTWARE_RENDERER_H

#include <cglm/cglm.h>

#include "renderer/renderer.h"
#include "mesh_instance.h"

#define XRES 320
#define YRES 200

typedef struct ScreenVert {
	uint16_t x, y;
	float u, v;
} ScreenVert;

typedef struct ScreenTri {
	ScreenVert verts[3];
	MeshInstance* mesh_instance;
	uint16_t avg_z;
} ScreenTri;

typedef struct SoftwareRenderer {
	uint8_t framebuffer[XRES * YRES];
	MeshInstance* mesh_instances;

	mat4 view;
	mat4 proj;
} SoftwareRenderer;

extern SoftwareRenderer rd;

#endif
