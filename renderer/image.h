#ifndef IMAGE_H
#define IMAGE_H

#include <stdlib.h>
#include <stdint.h>

#include "renderer.h"

typedef struct Image {
	uint32_t width, height;
	uint8_t *data;
} Image;

inline void image_from_raw(Image* image, uint8_t* data, uint32_t width, uint32_t height)
{
	image->width = width;
	image->height = height;
	image->data = data;
}

inline void image_uninit(Image* image)
{
	if (image) {
		image->width = 0;
		image->height = 0;
		if (image->data) {
			free(image->data);
			image->data = NULL;
		}
	}
}

#endif
