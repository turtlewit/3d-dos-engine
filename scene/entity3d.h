#ifndef ENTITY3D_H
#define ENTITY3D_H

#include "entity.h"
#include "core/transform.h"
#include "renderer/mesh_instance.h"

typedef struct Entity3D {
	COMPONENT_STRUCT(Entity3D)

	Transform transform;
	MeshInstance* mesh_instance;

	quat quaternion;
	vec3 scale;
} Entity3D;

COMPONENT_STRUCT_FUNCS(Entity3D, entity3d)

#endif
