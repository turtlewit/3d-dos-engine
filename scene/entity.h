#ifndef ENTITY_H
#define ENTITY_H

#include "core/list.h"

#define COMPONENT_STRUCT(type) \
	type* next; \
	type* prev; \

#define COMPONENT_STRUCT_FUNCS(type, prefix) \
	inline type* prefix_new(type** _head) \
	{\
		type* _r = malloc(sizeof(type));\
		list_add(_head, _r);\
		return _r;\
	}\
	inline type* prefix_delete(type** _head, type* _t)\
	{\
		list_remove(_head, _t);\
		free(_t);\
	}

#endif
