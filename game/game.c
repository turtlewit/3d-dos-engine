#include "core/game.h"

#include "core/file.h"

#include "renderer/palette.h"
#include "renderer/image.h"
#include "renderer/mesh.h"
#include "renderer/mesh_instance.h"
#include "platform/input.h"
#include "platform/keys.h"

Vertex triangle[] = {
	{1.0, 1.0, {0.5, -0.5, 0.0}},
	{0.5, 0.0, {0.0, 0.5, 0.0}},
	{0.0, 1.0, {-0.5, -0.5, 0.0}},
};

struct Game {
	Palette palette;
	Image parrot;
	Mesh* cube;
	MeshInstance* cube_instance;
	MeshInstance* cube_instance_2;

	struct {
		bool up;
		bool down;
		bool left;
		bool right;
	} input;
};

static Game _game;

Game* game_new(void)
{
	_game = (Game){};
	return &_game;
}

void game_init(Game* game)
{
	palette_init(&game->palette, file_load("assets/palettes/palette.pal"));
	image_from_raw(&game->parrot, file_load("assets/textures/parrot.raw"), 150, 200);

	palette_set_active(&game->palette);

	game->cube = mesh_new();
	mesh_init(game->cube, (Vertex*)file_load("assets/meshes/cube.bin"), 36);
	//mesh_init(game->cube, triangle, 3);
	game->cube_instance_2 = mesh_instance_init(game->cube, &game->parrot);
	game->cube_instance = mesh_instance_init(game->cube, &game->parrot);
	vec3 translation = {0.f, 0.f, 3.f};
	glm_translate(*mesh_instance_get_matrix(game->cube_instance_2), translation);

	vec3 eye = { 0.0f, 3.0f, -3.0f };
	vec3 center = { 0.0f, 0.0f, 0.0f };
	vec3 up = { 0.0f, 1.0f, 0.0f };
	glm_lookat(eye, center, up, *rd_get_view());

	glm_perspective(30, 6.f/5.f, 0.01f, 100.f, *rd_get_proj());
}

void game_tick(Game* game, float delta)
{
	//printf("tick\n");
	vec3 axis = {0.f, 1.f, 0.f};
	glm_rotate(*mesh_instance_get_matrix(game->cube_instance), delta, axis);

	{
		vec3 movement = {
			(float)game->input.left - (float)game->input.right,
			0.f,
			(float)game->input.down - (float)game->input.up,
		};
		glm_vec3_scale(movement, delta, movement);
		glm_translate(*rd_get_view(), movement);
	}
}

void game_input(Game* game, const InputEvent* event)
{
	switch (event->type) {
	
	case INPUT_EVENT_KEYBOARD: {
		switch (event->keyboard.key) {
		case KEY_W: {
			game->input.up = event->keyboard.pressed;
		} break;
		case KEY_S: {
			game->input.down = event->keyboard.pressed;
		} break;
		case KEY_A: {
			game->input.left = event->keyboard.pressed;
		} break;
		case KEY_D: {
			game->input.right = event->keyboard.pressed;
		} break;
		default: break;
		}
	} break;
	default: break;
	}
}

void game_uninit(Game* game)
{
	mesh_instance_uninit(game->cube_instance_2);
	mesh_instance_uninit(game->cube_instance);
	mesh_uninit(game->cube);
	//free(game->cube);
	image_uninit(&game->parrot);
	palette_uninit(&game->palette);
}
