#ifndef LIST_H
#define LIST_H

#define list_add(pp_head, p_element) \
	if (!*(pp_head)) { \
		*(pp_head) = p_element; \
		p_element->next = NULL; \
		p_element->prev = NULL; \
	} else { \
		(pp_head)[0]->prev = p_element; \
		(p_element)->next = (pp_head)[0]; \
		(p_element)->prev = NULL; \
		*(pp_head) = p_element; \
	}

#define list_remove(pp_head, p_element) \
	if ((pp_head)[0] == (p_element)) { \
		*(pp_head) = (p_element)->next; \
	} \
	if ((p_element)->next) { \
		(p_element)->next->prev = (p_element)->prev; \
	} \
	if ((p_element)->prev) { \
		(p_element)->prev->next = (p_element)->next; \
	}

#endif
