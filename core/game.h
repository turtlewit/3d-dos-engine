#ifndef GAME_H
#define GAME_H

typedef struct Game Game;
typedef struct InputEvent InputEvent;

Game* game_new(void);
void game_init(Game* game);
void game_tick(Game* game, float delta);
void game_input(Game* game, const InputEvent* event);
void game_uninit(Game* game);

#endif
