#ifndef SCENE_TRANSFORM_H
#define SCENE_TRANSFORM_H

#include <cglm/cglm.h>

typedef struct Transform {
	vec3 origin;
	mat3 basis;
} Transform;

#endif
