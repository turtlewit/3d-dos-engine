#include <time.h>
#include <stdio.h>
#include <string.h>

#include "mainloop.h"
#include "game.h"

#include "platform/platform.h"
#include "platform/input.h"
#include "renderer/renderer.h"

typedef struct Args {
	bool no_video;
} Args;

bool game_running = true;

void quit(void)
{
	game_running = false;
}

Args parse_args(int argc, char** argv)
{
	Args args = {0};
	for (int i = 0; i < argc; ++i) {
		if (strcmp(argv[i], "--no-video") == 0) {
			args.no_video = true;
		}
	}
	return args;
}

int mainloop(int argc, char** argv)
{
	Args args = parse_args(argc, argv);

	if (!args.no_video) {
		platform_init();
	}

	input_init();

	Game* game = game_new();

	game_init(game);

	clock_t frame_time = clock();
	//while (!input_is_char_available()) {
	while (game_running) {
		clock_t new_frame_time = clock();

		float delta = ((float) new_frame_time - frame_time) / (float) CLOCKS_PER_SEC;
		frame_time = new_frame_time;

		InputEvent event;
		while (input_pop_event(&event) || input_poll(&event)) {
			switch (event.type) {
			case INPUT_EVENT_KEYBOARD: {
				//printf("Keyboard event: scancode: %u, down: %d\n", event.keyboard.key, event.keyboard.pressed);
			} break;
			case INPUT_EVENT_KEYBOARD_MODIFIER: {
			} break;
			case INPUT_EVENT_MOUSE_BUTTON: {
			} break;
			case INPUT_EVENT_MOUSE_MOTION: {
			} break;
			case INPUT_EVENT_JOYSTICK_BUTTON: {
			} break;
			case INPUT_EVENT_JOYSTICK_AXIS: {
				if (event.joystick_axis.joystick == 0 && event.joystick_axis.axis == INPUT_JOYSTICK_AXIS_HORIZONTAL) {
					//printf("\r\033[Kjoystick value: %d", event.joystick_axis.value);
					//fflush(stdout);
				}
			} break;
			default: break;
			}

			game_input(game, &event);
		}

		game_tick(game, delta);

		rd_clear();
		rd_render();
		if (!args.no_video) {
			rd_present();
		}
	}

	game_uninit(game);

	if (!args.no_video) {
		input_uninit();
	}

	platform_uninit();

	return 0;
}
