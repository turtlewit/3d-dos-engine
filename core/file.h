#ifndef FILE_H
#define FILE_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// Returns a dynamically allocated buffer 
// containing the file bytes. The buffer
// must be freed by the caller.
inline uint8_t* file_load(const char* path) {
	uint8_t* file_buffer;

	FILE* file = fopen(path, "rb");
	fseek(file, 0, SEEK_END);
	long size = ftell(file);
	fseek(file, 0, SEEK_SET);
	file_buffer = malloc(size);
	fread(file_buffer, size, 1, file);

	return file_buffer;
}

#endif
