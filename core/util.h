#ifndef UTIL_H
#define UTIL_H

#define COORD_TO_INDEX(x, y, width) \
	((y * width) + x)

#define swap(a, b, type) \
	{ \
		type temp = a; \
		a = b; \
		b = temp; \
	}

#define clamp(a, min, max) ((a > min ? a : min) < max ? a : max)

#define in_range_inclusive(x, min, max) ((x >= min) && (x <= max))
#define in_range_exclusive(x, min, max) ((x > min) && (x < max))

#endif
