import bpy
from ctypes import c_float

def export(filename):
    verts = []
    obj = bpy.context.object
    for p in bpy.context.object.data.polygons:
        t = p.vertices
        for v in t:
            tv = obj.matrix_world @ obj.data.vertices[v].co
            verts.append([c_float(x) for x in tv])
    
    uvs = []
    for uv in obj.data.uv_layers[0].data:
        uvs.append([c_float(x) for x in uv.uv])
    
    with open(filename, 'wb') as f:
        l = [x for z in list(zip(uvs, verts)) for y in z for x in y]
        array = (c_float * len(l))(*l)
        f.write(bytes(array))